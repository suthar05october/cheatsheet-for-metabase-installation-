# Cheat Sheet Ubuntu 16.04

##  Install SSH On Ubuntu

##  login to ssh

- check you are on root or not
```
sudo su

Enter your password of sytem
```

##  Installing Java

- Install software-properties-common to easily add new repositories
```
sudo apt-get install software-properties-common

```
- Baisc Infomation
-this java command will show
-Do you want to continue (y/N)
-please press y


- Add the Java PPA:

```
sudo add-apt-repository ppa:webupd8team/java 

```

- Update the source list

```

sudo apt-get update

```
- Install the Java JDK 8:
```
 sudo apt-get install oracle-java8-installer
```
- Baisc Infomation
-then it will show two popup on terminal
-first you need to press ok
-second you need to press yes

# Install Mysql Server

- Download MySQL Server. Enter a root password when specified:

```
sudo apt install mysql-server

```

- Log in as the root user:

```
mysql -u root -p

```

- Create a database and user for Metabase:

```
CREATE DATABASE employees;
CREATE USER 'metabase_user' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON employees.* TO 'metabase_user';
GRANT RELOAD ON *.* TO 'metabase_user';
FLUSH PRIVILEGES;
quit

```

## Adding testing database in mysql directly from git
```
sudo apt install git
git clone https://github.com/datacharmer/test_db.git
cd test_db
mysql -u metabase_user -p employees < employees.sql

```
## Enter the password
```
    password
```
## Now creating metabase env file for that need to create metabase folder and env file to this folder /var/metabase/metabase-env
```
cd /var/
mkdir metabase
cd metabase
touch metabase-env
nano metabase-env
```
## Add variables to metabse-env
```
export MB_DB_TYPE=mysql
export MB_DB_DBNAME=employees
export MB_DB_PORT=3306
export MB_DB_USER=metabase_user
export MB_DB_PASS=password
export MB_DB_HOST=localhost
```
## Load these environment variables and it load in the current shell
```
    source metabase-env
```

#Download Metabase Permalink

- Download the jar file from Metabase

```

wget http://downloads.metabase.com/v0.28.1/metabase.jar

```
## update metabse verison and all the commands are need to change version of metabase jar
```

wget http://downloads.metabase.com/v0.31.0/metabase.jar

```
#Move the file into /var so that it can start on reboot

```
sudo mv metabase.jar /var/metabase.jar

```
## Metabase to Start at Reboot

```
 which java
 nano /etc/systemd/system/metabase.service
 ```
## add this in file
```
 [Unit]
    Description=Metabase server
    After=syslog.target
    After=network.target[Service]
    User=username
    Type=simple

[Service]
    ExecStart=/usr/bin/java -jar /var/metabase.jar
    Restart=always
    StandardOutput=syslog
    StandardError=syslog
    SyslogIdentifier=metabase

[Install]
    WantedBy=multi-user.target


```
## Then Apply the change
```
sudo systemctl start metabase
sudo systemctl status metabase

```

# Install ngnix with lets encrypt

```
apt -y install software-properties-common
apt -y install nginx
add-apt-repository --yes ppa:certbot/certbot
apt update
apt -y install certbot

```

#certbot certonly --webroot -w /var/www/html -d dap-udea.net 

# After installing you recive save this notepad


```
- Congratulations! Your certificate and chain have been saved at:
/etc/letsencrypt/live/dap-udea.net/fullchain.pem
Your key file has been saved at: /etc/letsencrypt/live/dap-udea.net/privkey.pem

```

## create a cron job for auto-renwing certifcate on root if cron file is not created on root or error is showing no cron tab for root

```
crontab -e
-then press 2 it will create cron file  
enter tis command in file
36 2 * * * /usr/bin/certbot renew --post-hook "systemctl reload nginx"

#( Note:- To edit Nano file Please Press Ctrl+O then Press Ctrl+M after this Press Ctrl+X )
it will  install cron job 


```


## Create a new Nginx server block configuration of the reverse proxy for Metabase

```
nano /etc/nginx/sites-available/metabase


#( Note:- To edit Nano file Please Press Ctrl+O then Press Ctrl+M after this Press Ctrl+X )

```

## Enter the following configuration in the editor. Replace all occurrences of the example domain with the actual one.replace

```
 /etc/letsencrypt/live/dap-udea.net/fullchain.pem;

 ssl_certificate_key/etc/letsencrypt/live/dap-udea.net/privkey.pem;
 
```


# With We Save When Our Certifcate Generate

```
server {
	listen 443;
	server_name dap-udea.net;
	ssl_certificate
	/etc/letsencrypt/live/dap-udea.net/fullchain.pem;
	ssl_certificate_key
	/etc/letsencrypt/live/dap-udea.net/privkey.pem;
	ssl on;
	ssl_session_cache builtin:1000 shared:SSL:10m;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;
	ssl_prefer_server_ciphers on;
	gzip on;
	gzip_http_version 1.1;
	gzip_vary on;
	gzip_comp_level 6;
	gzip_proxied any;
	gzip_types text/plain text/html text/css application/json application/javascript
	application/x-javascript text/javascript text/xml application/xml application/rss+xml
	application/atom+xml application/rdf+xml;
	gzip_buffers 16 8k;
	gzip_disable “MSIE [1-6].(?!.*SV1)”;
	access_log /var/log/nginx/metabase.access.log;
	location / {
		proxy_pass
		http://localhost:3000;
		proxy_set_header host $host;
		proxy_http_version 1.1;
		proxy_set_header upgrade $http_upgrade;
		proxy_set_header connection "upgrade";
	}
}

```

- Activate the configuration file by running.

```
ln -s /etc/nginx/sites-available/metabase /etc/nginx/sites-enabled/metabase

```

- You can verify if the configuration file is error free by running nginx -t.

```
root@aliyun:~# nginx -t

```

- Restart the Nginx web server so that the change in configuration can be applied

```
systemctl restart nginx

```
##Adjusting the Firewall Rules
## check status of firewall

```
sudo ufw status

```
## Status is not active then enable it on your server it is off you need to enable it is already install in our server

```
sudo ufw enable
sudo ufw status

```
## Enable some firewal extensions

```
sudo ufw allow http
sudo ufw allow https
sudo ufw allow OpenSSH
```
## Infomation for another server if ufw not install you need to type
```
sudo apt-get install ufw
sudo ufw status
sudo ufw allow http
sudo ufw allow https
sudo ufw allow OpenSSH
```


#You can now access your Metabase instance through your favorite browser by browsing “https://dap-udea.net”. You should see the welcome screen from Metabase. Provide the basic information about the administrator account and your organization.